# bitmap2image

Takes a plaintext bitmap file and converts it into an Ebitengine image that can be saved out as different image formats.


# Usage:
```
bitmap2image inputfile.sprite
```

```
╰─$ ./bitmap2image ebiten.sprite
Sprite:
----O-
----OO
--OO--
-OOO--
OOO---
OO----

x: 6 y: 6
```
![alt text](screenshot.png "Demo Screenshot")
