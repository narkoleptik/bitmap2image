package main

import (
	"errors"
	"fmt"
	"image/color"
	"image/gif"
	"image/jpeg"
	"image/png"
	"math"
	"os"
	"strings"

	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/ebitenutil"
	"github.com/hajimehoshi/ebiten/v2/inpututil"
	"golang.org/x/image/colornames"
)

const (
	screenWidth  = 800
	screenHeight = 600
	gameTitle    = "Helper"
)

var (
	ebitenOrange     = color.RGBA{205, 95, 50, 255}
	spriteFile       string
	spriteName       []string
	sprite           []string
	spriteW, spriteH int
	spriteAngle      = 0

	ebImage    *ebiten.Image
	infoBox    *ebiten.Image
	fileSaved  = false
	debugLines = false
	scale      = 10.0
)

type Game struct{}

func (g *Game) Layout(_, _ int) (w, h int) {
	return screenWidth, screenHeight
}

// Update proceeds the game state.
// Update is called every tick (1/60 [s] by default).
func (g *Game) Update() error {
	if inpututil.IsKeyJustPressed(ebiten.KeyQ) {
		return errors.New("game shutdown")
	}
	if inpututil.IsKeyJustPressed(ebiten.KeyD) {
		debugLines = !debugLines
	}
	if ebiten.IsKeyPressed(ebiten.KeyArrowUp) {
		scale++
	}
	if ebiten.IsKeyPressed(ebiten.KeyArrowDown) {
		scale--
		if scale < 1 {
			scale = 1
		}
	}
	if ebiten.IsKeyPressed(ebiten.KeyLeft) {
		spriteAngle--
		if spriteAngle < 0 {
			spriteAngle = 360
		}
	}
	if ebiten.IsKeyPressed(ebiten.KeyRight) {
		spriteAngle++
		if spriteAngle > 360 {
			spriteAngle = 0
		}
	}
	if inpututil.IsKeyJustPressed(ebiten.KeyP) {
		writeImage("png")
	}
	if inpututil.IsKeyJustPressed(ebiten.KeyJ) {
		writeImage("jpg")
	}
	if inpututil.IsKeyJustPressed(ebiten.KeyG) {
		writeImage("gif")
	}

	return nil
}

func (g *Game) Draw(screen *ebiten.Image) {
	// Write your game's rendering.
	//screen = screens[currentScreen]
	if debugLines {
		// crosshairs for debugging
		for i := 0; i < screenHeight; i++ {
			screen.Set(screenWidth/2, i, colornames.Blue)
			screen.Set(screenWidth/2-1, i, colornames.Blue)
		}

		for i := 0; i < screenWidth; i++ {
			screen.Set(i, screenHeight/2, colornames.Blue)
			screen.Set(i, screenHeight/2-1, colornames.Blue)
		}
	}

	// Display sprite in the center of the screen
	spriteW, spriteH := spriteSize(sprite)
	gx, gy := screenWidth/2.0, screenHeight/2.0
	op := &ebiten.DrawImageOptions{}
	// move the image to be centered on 0,0
	op.GeoM.Translate(-float64(spriteW)/2, -float64(spriteH)/2)
	// rotate the image
	op.GeoM.Rotate(float64(spriteAngle%360) * 2 * math.Pi / 360)

	op.GeoM.Scale(scale, scale)
	op.GeoM.Translate(gx, gy)
	screen.DrawImage(ebImage, op)

	infoBox.Clear()
	text := fmt.Sprintf("Scale (up/down): %.0f Sprite: %dx%d Rotate (left/right): %d\n", scale, spriteW, spriteH, spriteAngle)
	text = fmt.Sprintf("%sD)ebug crosshairs\nQ)uit\n", text)
	text = fmt.Sprintf("%sSave as:\nP) %s.png\nJ) %s.jpg\nG) %s.gif", text, spriteName[0], spriteName[0], spriteName[0])
	ebitenutil.DebugPrint(infoBox, text)

	infoOp := &ebiten.DrawImageOptions{}
	_, bh := infoBox.Size()
	infoOp.GeoM.Translate(20, float64(screenHeight-bh))
	screen.DrawImage(infoBox, infoOp)
}

func main() {
	progname := strings.Split(os.Args[0], "/")
	if len(os.Args) < 2 {
		fmt.Printf("Usage: %s inputfile.sprite\n", progname[len(progname)-1])
		os.Exit(0)
	} else {
		spriteFile = os.Args[len(os.Args)-1]
		spriteName = strings.Split(spriteFile, ".")
	}

	dat, err := os.ReadFile(spriteFile)
	check(err)
	sprite = strings.Split(string(dat), "\n")
	fmt.Println("Sprite:")
	for _, l := range sprite {
		fmt.Println(l)
	}

	w, h := spriteSize(sprite)
	fmt.Println("x:", w, "y:", h)
	infoBox = ebiten.NewImage(screenWidth, screenHeight*.25)
	ebImage = ebiten.NewImage(w, h)
	ebImage.Clear()

	for l, line := range sprite {
		for p := 0; p < len(line); p++ {
			if string(line[p]) != "-" {
				pixel := ebitenOrange
				switch string(line[p]) {
				case "R":
					pixel = colornames.Red
				case "O":
					if spriteName[0] == "ebiten" {
						pixel = ebitenOrange
					} else {
						pixel = colornames.Orange
					}
				case "Y":
					pixel = colornames.Yellow
				case "N":
					pixel = colornames.Green
				case "L":
					pixel = colornames.Lime
				case "X":
					pixel = colornames.Black
				case "B":
					pixel = colornames.Blue
				case "P":
					pixel = colornames.Purple
				case "I":
					pixel = colornames.Indigo
				case "V":
					pixel = colornames.Violet
				case "W":
					pixel = colornames.White
				case "g":
					pixel = colornames.Lightgray
				case "G":
					pixel = colornames.Gray
				default:
					pixel = colornames.White
				}
				ebImage.Set(p, l, pixel)
			}
		}
	}

	game := &Game{}
	ebiten.SetWindowSize(screenWidth, screenHeight)
	ebiten.SetWindowTitle(gameTitle)

	// Call ebiten.RunGame to start your game loop.
	if err := ebiten.RunGame(game); err != nil {
		if err.Error() == "game shutdown" {
			os.Exit(0)
		} else {
			check(err)
		}
	}

}

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func spriteSize(i []string) (int, int) {
	h := len(i) - 1
	w := len(i[0])
	return w, h
}

func writeImage(t string) {
	fn := fmt.Sprintf("%s.%s", spriteName[0], t)
	f, err := os.Create(fn)
	check(err)
	defer f.Close()
	switch t {
	case "png":
		// Encode to `PNG` with `DefaultCompression` level
		// then save to file
		err = png.Encode(f, ebImage)
		check(err)
		if err == nil {
			fileSaved = true
			fmt.Printf("Saving %s\n", fn)
		}
	case "jpg":
		// Specify the quality, between 0-100
		// Higher is better
		opt := jpeg.Options{
			Quality: 100,
		}
		err = jpeg.Encode(f, ebImage, &opt)
		check(err)
		if err == nil {
			fileSaved = true
			fmt.Printf("Saving %s\n", fn)
		}
	case "gif":
		opt := gif.Options{
			NumColors: 256,
			// Add more parameters as needed
		}

		err = gif.Encode(f, ebImage, &opt)
		check(err)
		if err == nil {
			fileSaved = true
			fmt.Printf("Saving %s\n", fn)
		}
	default:

	}

}
